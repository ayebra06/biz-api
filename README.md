Instructions

* Navigate into /biz-api directory.
* Run the following in terminal assuming you have ruby on rails configured on your system.

- $ rails db:migrate && rake import_business_csv_data
- $ rails s

* To see all entries navigate to:
http://localhost:3000/businesses

* To see entries by id, navigate to:
http://localhost:3000/businesses/1

* Pagination 50 businesses per page.
http://localhost:3000/businesses?page=3
http://localhost:3000/businesses?page=4
