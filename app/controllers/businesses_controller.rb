class BusinessesController < ApplicationController
	def index
		@businesses = Business.all
		paginate json: @businesses, status: :ok, per_page: 50
	end

	def show
		@business = Business.find(params[:id])
		render json: @business, status: :ok
	end
end
