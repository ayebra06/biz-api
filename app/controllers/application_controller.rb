class ApplicationController < ActionController::API
	before_action :check_api_key

private
	def check_api_key
		api_token = request.headers['HTTP_API_KEY']
		user = User.find_by(api_token: api_token)
		if !user
			render json: {status: 401, message: 'Unauthorized User'}, status: 401
		end
	end
end
