class UsersController < ApplicationController
	def show
		api_token = request.headers['HTTP_API_KEY']
		@user = User.find(params[:id])
		if @user.api_token == api_token
			render json: @user, status: :ok
		else
			render json: {status: 401, message: 'Unauthorized User'}, status: 401
		end
	end
end
