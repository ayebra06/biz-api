require 'csv'
require 'stringio'
require 'zlib'

desc "Import business csv data from source url"
task :import_business_csv_data => :environment do
	url = 'https://s3.amazonaws.com/ownlocal-engineering/engineering_project_businesses.csv.gz'
	csv_data = open(url).read
	decompressed = Zlib::GzipReader.new(StringIO.new(csv_data)).read
	count = 0
	CSV.parse(decompressed, headers: true) do |row|
		business_hash = row.to_hash
		business = Business.create(business_hash)
		puts "Created business num #{count}"
		count += 1
	end
end
